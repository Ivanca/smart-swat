﻿// Just add this script to your camera. It doesn't need any configuration.

using UnityEngine;


public class EventManager
{
    public delegate void TouchAction();
    public static event TouchAction OnTouch;

    public static void TriggerOnTouch()
    {
        if(OnTouch != null)
            OnTouch();
    }
}



public class TouchCamera : MonoBehaviour {


    private float perspectiveZoomSpeed = 0.5f;

    const float MIN_X_POSITION = 24.4f;
    const float MIN_Y_POSITION = 10f;
    const float MIN_Z_POSITION = -33f;

    Vector3 defaultDistance;
    float defaultDistanceMagnitude;

    Vector2?[] oldTouchPositions = {
		null,
		null
	};
	Vector2 oldTouchVector;

    float oldTouchDistance;

    void Start()
    {
        EventManager.TriggerOnTouch();
        // Default distance is the position
        // cause it is looking at 0,0,0 at the start
        defaultDistance = transform.position;
        defaultDistanceMagnitude = (new Vector3(transform.position.x, 0, transform.position.z)).magnitude;
        transform.LookAt(Vector3.zero);
        //defaultDistanceMagnitude = defaultDistance.magnitude;
    }

	void Update() {
        
        #if UNITY_EDITOR
            TouchSimulation();
        #else
            HandleTouch(Input.touches);
        #endif
    }



#if UNITY_EDITOR
    Vector3 lastMousePosition;
    Vector2 temp = new Vector2(1f, 0);
    void TouchSimulation()
    {
        var touchCreator = new TouchCreator();
        if (Input.GetMouseButton(0)) {
            //Touch fakeTouch = new Touch();
            touchCreator.fingerId = 10;
            touchCreator.position = Input.mousePosition;
            touchCreator.deltaTime = Time.deltaTime;
            touchCreator.deltaPosition = Input.mousePosition - lastMousePosition;
            touchCreator.phase = (Input.GetMouseButtonDown(0) ? TouchPhase.Began :
                                (touchCreator.deltaPosition.sqrMagnitude > 1f ? TouchPhase.Moved : TouchPhase.Stationary));
            touchCreator.tapCount = 1;
            var touches = new Touch[1];
            touches[0] = touchCreator.Create();
            // left control to simulate 2 finger touch
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftAlt))
            {
                //print("2 touches!");
                touches = new Touch[2];
                touches[0] = touchCreator.Create();

                if (Input.GetKey(KeyCode.LeftAlt) && temp.x > 1f) {
                    temp -= new Vector2(0.2f, 0f);
                } else if (Input.GetKey(KeyCode.LeftControl) && temp.x < 1000) {
                    temp += new Vector2(0.2f, 0f);
                } else {
                    return;
                }
                touchCreator.position += temp;
                touches[1] = touchCreator.Create();
            }

            HandleTouch(touches);
        } else
        {
            var touches = new Touch[0];
            HandleTouch(touches);
        }
        lastMousePosition = Input.mousePosition;

    }
#endif

    private float currentAngle = 225;
    void HandleTouch  (Touch[] touches) {

        if (touches.Length == 0) {
			oldTouchPositions[0] = null;
			oldTouchPositions[1] = null;
		}
		else if (touches.Length == 1) {
			if (oldTouchPositions[0] == null || oldTouchPositions[1] != null) {
				oldTouchPositions[0] = touches[0].position;
				oldTouchPositions[1] = null;
			}
			else {
				Vector2 newTouchPosition = touches[0].position;
                Vector3 move = (Vector3)((oldTouchPositions[0] - newTouchPosition) * GetComponent<Camera>().orthographicSize / GetComponent<Camera>().pixelHeight * 2f);
                if (move.x != 0)
                {
                    currentAngle += move.x * 10f;
                    Quaternion q = Quaternion.Euler(0, currentAngle, 0);
                    Vector3 direction = q * Vector3.forward;
                    Vector3 diff = transform.position;
                    
                    float targetPos = transform.position.y - defaultDistance.y;
                    transform.position = - direction * defaultDistanceMagnitude + new Vector3(0, transform.position.y, 0);
                    transform.LookAt(new Vector3(0f, targetPos, 0f));
                    move.x = 0;
                }
                transform.position += transform.TransformDirection(move);
                oldTouchPositions[0] = newTouchPosition;
			}
		}
		else {
            Camera camera = GetComponent<Camera>();

            if (oldTouchPositions[1] == null) {
				oldTouchPositions[0] = touches[0].position;
				oldTouchPositions[1] = touches[1].position;
				oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
				oldTouchDistance = oldTouchVector.magnitude;
                
			} else {
				Vector2 screen = new Vector2(camera.pixelWidth, camera.pixelHeight);
				
				Vector2[] newTouchPositions = {
                    touches[0].position,
                    touches[1].position
				};
				Vector2 newTouchVector = newTouchPositions[0] - newTouchPositions[1];
                float newTouchDistance = newTouchVector.magnitude;
                if (newTouchVector != Vector2.zero)
                {
                    print("OK! " + newTouchVector);
				    // transform.position += transform.TransformDirection((Vector3)((oldTouchPositions[0] + oldTouchPositions[1] - screen) * GetComponent<Camera>().orthographicSize / screen.y));
                    float newZ = Mathf.Asin(Mathf.Clamp((oldTouchVector.y * newTouchVector.x - oldTouchVector.x * newTouchVector.y) / oldTouchDistance / newTouchDistance, -1f, 1f)) / 0.0174532924f;

                    // Otherwise change the field of view based on the change in distance between the touches.
                    camera.fieldOfView += (oldTouchDistance - newTouchDistance) * perspectiveZoomSpeed;
                    
                    // Clamp the field of view to make sure it's between 0 and 180.
                    camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 0.1f, 179.9f);
                    // GetComponent<Camera>().orthographicSize *= oldTouchDistance / newTouchDistance;
                    // transform.position -= transform.TransformDirection((newTouchPositions[0] + newTouchPositions[1] - screen) * GetComponent<Camera>().orthographicSize / screen.y);
                }
                oldTouchPositions[0] = newTouchPositions[0];
				oldTouchPositions[1] = newTouchPositions[1];
				oldTouchVector = newTouchVector;
				oldTouchDistance = newTouchDistance;

			}
		}
        if (false && touches.Length != 0)
        {
            if (transform.position.x < MIN_X_POSITION)
            {
                transform.position = new Vector3(MIN_X_POSITION, transform.position.y, transform.position.z);
            }
            if (transform.position.y < MIN_Y_POSITION)
            {
                transform.position = new Vector3(transform.position.x, MIN_Y_POSITION, transform.position.z);
            }
            if (transform.position.z < MIN_Z_POSITION)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, MIN_Z_POSITION);
            }

        }
    }




}
