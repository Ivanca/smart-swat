﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]
public class WaypointNavigation : MonoBehaviour {


    public List<Transform> waypoint;
    public float speed = 0.1f;
    private int currentWaypoint = 0;
    public bool loop = true;
    public Transform player;
    private CharacterController character;

    void Start() {
        character = GetComponent<CharacterController>();
        //waypoint = new List<Transform>();
    }

    void Update() {
        // Code taken from http://answers.unity3d.com/questions/139191/make-ai-move-using-charactercontrollermove-instead.html

        if (currentWaypoint < waypoint.Count) {
            Vector3 target = waypoint[currentWaypoint].position;
            target.y = transform.position.y; // keep waypoint at character's height
            Vector3 moveDirection = target - transform.position;
            moveDirection.y = 0;
            if (moveDirection.magnitude < 1) {
                print("moving 3");
                transform.position = target; // force character to waypoint position
                currentWaypoint++;
            } else {
                
                print(moveDirection.normalized + " " + speed + " " + Time.deltaTime);
                print(moveDirection.normalized * speed * Time.deltaTime);
                transform.LookAt(target);
                character.Move(moveDirection.normalized * speed * Time.deltaTime);
            }
        } else if (loop) {
            currentWaypoint = 0;
        }
    }
}
