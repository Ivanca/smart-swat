﻿using UnityEngine;
using System.Collections;

public class MoveToPointerPositionOnGrid : MonoBehaviour {

    // Use this for initialization

    LayerMask layerMask;

    void Start () {
        layerMask = LayerMask.GetMask("Active Floor");
    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f, layerMask))
        {
            //StartCoroutine(ScaleMe(hit.transform));
            Vector3 newSquarePos = hit.point;
            newSquarePos.y += 0.01f;
            //Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object
            transform.position = SnapToGrid(newSquarePos);
        }

    }

    Vector3 SnapToGrid(Vector3 pos)
    {
        int gridSnap = 1;
        Vector3 snapHits = new Vector3(Mathf.Round((pos.x - 0.5f) / gridSnap) * gridSnap + 0.5f, pos.y, Mathf.Round((pos.z - 0.5f) / gridSnap) * gridSnap + 0.5f);
        Vector3 snapTransform = new Vector3(snapHits.x, pos.y, snapHits.z);
        pos = snapTransform;
        return pos;
    }

}
