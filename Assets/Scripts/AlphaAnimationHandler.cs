﻿using UnityEngine;
using System.Collections;

public class AlphaAnimationHandler : MonoBehaviour
{
    Color startColor;
    Color currentColor;
    Color endColor;
    bool shouldFade = false;
    float startTime;
    public float duration = 0.2f;
    float t;
    bool hasRenderer = true;
    float fadedOpacity = 0.1f;

    // Use this for initialization
    void Start()
    {
        if (gameObject.GetComponent<Renderer>() == null)
        {
            hasRenderer = false;
        }

        if (hasRenderer)
        {
            startColor = gameObject.GetComponent<Renderer>().material.color;
            endColor = new Color(startColor.r, startColor.g, startColor.b, fadedOpacity);
            // To start all faded (so no fading-out animation)
            convertMaterialToFade(gameObject.GetComponent<Renderer>().material);
            gameObject.GetComponent<Renderer>().material.SetColor("_Color", endColor);
        }

        for (int childIndex = 0; childIndex < gameObject.transform.childCount; childIndex++)
        {
            Transform child = gameObject.transform.GetChild(childIndex);
            child.gameObject.AddComponent<AlphaAnimationHandler>();
        }
    }

    void OnEnable()
    {
        ActivateOverlappingFloorOnly.OnActivateFloor += OnActivateFloor;
        ActivateOverlappingFloorOnly.OnDeactivateFloor += OnDeactivateFloor;
    }

    void OnDisable() {
        ActivateOverlappingFloorOnly.OnActivateFloor -= OnActivateFloor;
        ActivateOverlappingFloorOnly.OnDeactivateFloor -= OnDeactivateFloor;
    }

    void OnActivateFloor(Transform floorContainer)
    {
        if (ItIsOneOfItsParents(floorContainer))
        {
            //print("Toggling true");
            toggle(true);
        }
    }

    void OnDeactivateFloor(Transform floorContainer)
    {
        if (ItIsOneOfItsParents(floorContainer))
        {
            //print("Toggling false");
            toggle(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hasRenderer)
        {
            Fade();
        }
    }

    bool fadingIn = false;

    void Fade()
    {
        if (shouldFade)
        {
            t = (Time.time - startTime) / duration;

            if (fadingIn)
            {
                currentColor = Color.Lerp(endColor, startColor, t);
            } else
            {
                currentColor = Color.Lerp(startColor, endColor, t);
            }
           // print(currentColor.a);
            gameObject.GetComponent<Renderer>().material.SetColor("_Color", currentColor);

            if (t >= 1)
            {
                shouldFade = false;
                if (fadingIn) {
                    convertMaterialToCutout(gameObject.GetComponent<Renderer>().material);
                }
            }
        }
    }

    void toggle(bool fadingInParam)
    {
        shouldFade = true;
        startTime = Time.time;
        fadingIn = fadingInParam;
        if (hasRenderer)
        {
            convertMaterialToFade(gameObject.GetComponent<Renderer>().material);
        }
    }

    void convertMaterialToFade(Material material)
    {
		material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
		material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
		material.SetInt("_ZWrite", 0);
		material.DisableKeyword("_ALPHATEST_ON");
		material.EnableKeyword("_ALPHABLEND_ON");
		material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
		material.renderQueue = 3000;
    }

    void convertMaterialToCutout(Material material)
    {
             material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
             material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
             material.SetInt("_ZWrite", 1);
             material.DisableKeyword("_ALPHATEST_ON");
             material.DisableKeyword("_ALPHABLEND_ON");
             material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
             material.renderQueue = -1;
    }

    bool ItIsOneOfItsParents(Transform parent)
    {
        Transform selfparent = transform.parent;
        while (selfparent != null)
        {
            if (selfparent == parent)
            {
                return true;
            }
            selfparent = selfparent.parent;
        }
        return false;
    }

}
