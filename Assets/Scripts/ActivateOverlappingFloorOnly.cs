﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivateOverlappingFloorOnly : MonoBehaviour {

    public delegate void ActivateFloorAction(Transform floorContainer);
    public static event ActivateFloorAction OnActivateFloor;

    public delegate void DeactivateFloorAction(Transform floorContainer);
    public static event DeactivateFloorAction OnDeactivateFloor;

    private GameObject activeFloor;
    private GameObject[] allFloors;

    int FLOOR;
    int ACTIVE_FLOOR;

    // Use this for initialization
    void Awake() {
        FLOOR = LayerMask.NameToLayer("Floor");
        ACTIVE_FLOOR = LayerMask.NameToLayer("Active Floor");
    }

    IEnumerator InitialFakeEventFiring() {
        // Give time to other scripts to to their `Awake()` and `Start()`
       yield return new WaitForSeconds(0.1f);
       allFloors = FindGameObjectsWithLayer(FLOOR);
       activeFloor = FindGameObjectsWithLayer(ACTIVE_FLOOR)[0];
       foreach (var floor in allFloors)
       {
           OnDeactivateFloor(floor.transform.parent);
       }
       OnActivateFloor(activeFloor.transform.parent);
    }

    void Start() {
        StartCoroutine(InitialFakeEventFiring());
    }

     void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == FLOOR) {
            if (activeFloor && OnDeactivateFloor != null) {
                activeFloor.layer = FLOOR;
                OnDeactivateFloor(activeFloor.transform.parent);
            }
            other.gameObject.layer = ACTIVE_FLOOR;
            activeFloor = other.gameObject;
            if (OnActivateFloor != null)
                OnActivateFloor(other.gameObject.transform.parent);
        }
    }

    GameObject[] FindGameObjectsWithLayer(int layer) {
        var goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        var goList = new List<GameObject> ();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }
        return goList.ToArray();
    }

}
